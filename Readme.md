# Toggle Split Pane

A [Sublime Text 3](http://www.sublimetext.com/) plugin.

Toggle vertical split view into the current buffer. Depends on Origami plugin.

## How to use

- Linux/Window
	- ctrl + p
- OS X
	-  + p