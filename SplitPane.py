import sublime_plugin

class SplitPaneCommand(sublime_plugin.WindowCommand):
   def run(self):
      w = self.window
      if w.num_groups() == 1:
         w.run_command('toggle_minimap')
         w.run_command('set_layout',      {
         "cols": [0.0, 0.5, 1.0],
         "rows": [0.0, 1.0],
         "cells": [[0, 0, 1, 1], [1, 0, 2, 1]]
         })

         w.run_command("travel_to_pane",  {"direction": "left"})     # Depends on Origami plugin.
         w.run_command('clone_file_to_pane', {"direction": "right"}) # Depends on Origami plugin.

         #w.focus_group(2)
         #w.run_command('clone_file_to_pane', {"direction": "right"})
#         w.run_command('move_to_group', {'group': 1})
#         w.run_command('clone_file')
      else:
         w.focus_group(1)
         w.run_command('close')
         w.run_command('set_layout', {
            'cols': [0.0, 1.0],
            'rows': [0.0, 1.0],
            'cells': [[0, 0, 1, 1]]
         })
         w.run_command('toggle_minimap')


         #ccc     "args": {"direction": "left"},
      #"command": "clone_file_to_pane" 